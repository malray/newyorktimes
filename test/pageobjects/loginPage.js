import Page from './page'

/**
 * Represents the login page.
 */
class LoginPage extends Page {

	/**
	 * define elements
	 */
	get usernameInput() { return browser.element('//*[@name="email_address"]'); }
	get passwordInput() { return browser.element('//*[@name="password1"]'); }
	get confirmPasswordInput() { return browser.element('//*[@name="password2"]'); }
	get createAccountBtn() { return browser.element('//span[contains(., "Create Account")]'); }
	get email() { return browser.element('//*[@name="userid"]'); }
	get password() { return browser.element('//*[@name="password"]'); }
	get loginBtn() { return browser.element('//span[contains(., "Log in")]'); }
	get loginLink() { return browser.element('//span[contains(., "Log In")]'); }
	get signupLink() { return browser.element('//span[contains(., "Sign up.")]'); }
	
	open() {
		super.open('https://cooking.nytimes.com/')
	    browser.pause(1000);
	}

	/**
	 * clicks the login button to either login or sign-up. 
	 */
	clickLoginLink() {
		this.loginLink.click();
	}
	
	/**
	 * clicks the sign-up link to sign up
	 */
	clickSignupLink() {
		this.signupLink.click();
	}
	
	/**
	 * Inputs the data specified to sign up and clicks the create 
	 * account button.
	 * 
	 * @param username
	 * @param password
	 */
	createAccount(username, password) {
		this.usernameInput.setValue(username);
		this.passwordInput.setValue(password);
		this.confirmPasswordInput.setValue(password);
		this.createAccountBtn.click();
	}
	
	/**
	 * logs into account
	 * 
	 * @param username
	 * @param password
	 */
	loginAccount(username, password) {
		this.clickLoginLink() 
		this.email.setValue(username);
		this.password.setValue(password);
		this.loginBtn.click();
	}
}

export default new LoginPage();