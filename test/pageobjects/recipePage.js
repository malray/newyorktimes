import Page from './page'

const recipeOfTheDayText = ''

/**
 * Represents the recipe page.
 */
class RecipePage extends Page {
	
	constructor() {
		super();
		this._recipeOfTheDayTxt = '';
	}
	
	/**
	 * define elements
	 */
	get saveRecipe() { return browser.element('//*[@class="recipe-save"]'); }
    get yourRecipeHombox() { return browser.element('//*[@class="nytc---loginbtn---navSubLabel"]'); }
    get saveRecipeLeftNav() { return browser.element('//span[contains(., "Saved Recipes")]'); }
    get recipeOfTheDayText() { return browser.element(
    		'/html/body/div[5]/div/div[1]/article/a/div[2]/div[1]/h3'
    		); }
    get savedRecipe() {
    		return browser
    			.element(
    					'/html/body/div[5]/div/div/div[2]/div/div/div[2]/div/div/article/a/h3'
    					); 
    	}
    
    get recipeOfTheDayTxt() { return this._recipeOfTheDayTxt; }
    set recipeOfTheDayTxt(value) { this.recipeOfTheDay = value; }
    
	/**
	 * saves recipe of day
	 */
	clickSaveRecipe() {
		this._recipeOfTheDayTxt = this.recipeOfTheDayText.getText();
		this.saveRecipe.click();
	}
	
	/**
	 * clicks on recipe homebox link.
	 */
	clickRecipeHomebox() {
		this.yourRecipeHombox.click();
	}
	
	/**
	 * clicks save recipe on left nav bar.
	 */
	clickSaveRecipLeftNav() {
		this.saveRecipeLeftNav.click();
	}
}

export default new RecipePage();