Feature: Create a new user

    As a new user on the NYTimes Cooking page
    I want to create a new user
    Because I want to save a recipe

    Background:

        Given I am on the NYTimes cooking page

    Scenario Outline: Performing create new user.
    		When I click on log in
    		When I click on sign up
        When I create account with username and password <user> <password>
        Then An account should be created

        Examples:
        |user| |password|
        |"<enter test user>"| |"<enter test user password>"|
        
