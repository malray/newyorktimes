Feature: Saving/Unsaving a recipe

    As a user on the NYTimes Cooking page
    I want to be able to save and unsave
    a recipe

    Background:

        Given I am on the NYTimes cooking page

    Scenario Outline: Saving a recipe.
        When I login with username and password <user> <password>
    		When I save a recipe from homepage
    		When I click Your Recipe Box on top navigation bar
        When I click on Saved Recipes on side left nav bar
        Then I will see the saved recipe

        Examples:
        |user| |password|
        |"<enter test user>"| |"<enter test user password>"|

    Scenario Outline: Un-save a recipe.
        When I login with username and password <user> <password>
    		When I save a recipe from homepage
    		When I click Your Recipe Box on top navigation bar
		When I click on Saved Recipes on side left nav bar
        When I will see the saved recipe
        Then I will unsaved the recipe via api call
        
        Examples:
        |user| |password|
        |"<enter test user>"| |"<enter test user password>"|
        
