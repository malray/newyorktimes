import { defineSupportCode } from 'cucumber';
import loginPage from '../pageobjects/loginPage';
import recipePage from '../pageobjects/recipePage'

defineSupportCode(function({ Then }) {
 
	Then(/^An account should be created$/, function() {
		recipePage.yourRecipeHombox.isVisible().should.
			equal('true');
	});
	
	Then(/^I will see the saved recipe$/, function() {
		recipePage.recipeOfTheDayTxt.
			should.equal(recipePage.savedRecipe.getText());
	});
	
	Then(/^I will unsaved the recipe via api call$/, function() {
		// var userId = '84819519';
		// var recipeId = '1016084';
		var finalUrl = 'https://cooking.nytimes.com/api/v2/users/84819519/collectables/recipe/1016084?includes=recipe,external_recipe'
		var XMLHttpRequest = require('xhr2');
		var xhr = new XMLHttpRequest();
		
		xhr.withCredentials = true;
		xhr.addEventListener("readystatechange", function () {
		  if (this.readyState === 4) {
		    console.log('it worked');
		  }
		});
		xhr.open("DELETE", finalUrl);	
		xhr.send();
	});
});
