import { defineSupportCode } from 'cucumber';
import loginPage from '../pageobjects/loginPage';

defineSupportCode(function({ Given }) {

  Given(/^I am on the NYTimes cooking page$/, function() {
	loginPage.open();
    browser.getTitle().should.equal(
    		'Cooking with the New York Times - NYT Cooking');
  });
  
});
