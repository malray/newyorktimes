import { defineSupportCode } from 'cucumber';
import loginPage from '../pageobjects/loginPage';
import recipePage from '../pageobjects/recipePage'

defineSupportCode(function({ When }) {
	
	// create a user feature.
	When(/^I click on log in$/, function() {
	   loginPage.clickLoginLink();
	});
	
	When(/^I click on sign up$/, function() {
	    loginPage.clickSignupLink();
	});
	
	When(/^I create account with username and password "([^"]*)" "([^"]*)"$/, 
			function(username, password) {
	    loginPage.createAccount(username, password);
	});
	
	// Save recipe feature.
	When(/^I login with username and password "([^"]*)" "([^"]*)"$/, 
			function(username, password) {
		if(loginPage.loginLink.isVisible()) {
			loginPage.loginAccount(username, password);
		}
	});
	
	When(/^I save a recipe from homepage$/, function() {
		if(recipePage.saveRecipe.isVisible()) {
			recipePage.clickSaveRecipe();
		}
	});
	
	When(/^I click Your Recipe Box on top navigation bar$/, function() {
		recipePage.clickRecipeHomebox();
	});
	
	When(/^I click on Saved Recipes on side left nav bar$/, function() {
		recipePage.clickSaveRecipLeftNav();
	});
	
});



