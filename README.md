README:

How to run:
Install JAVA. Java is needed to start the Selenium Server.
Install node.js
cd to where 'package.json' is located and run 'npm install' to obtain all dependencies/

In order to run the test you must have selenium server up so you can execute WebdriverIO test.
So launch selenium.

No you can run the test with the following command
$ ~/node_modules/webdriverio/bin/wdio test/config/wdio.conf.js

Assumptions:
The implementation was done with the following assumptions. For the create a new user test, I left a place in the feature file for you to put in a valid user name and password so the user can be created. In an ideal system the users within the db can empty out so each run will be in a clean state. 
The same assumption goes for the save recipe feature. You need to enter the user of your choice that you would like to run with in the feature file where it's designated.
Test were also written under with the assumption that each run be in a clean state. 

::::::NOTE::::::::

I could not get the third task to work completely. I tried everything except JQuery and I could not figure out how to set the cookie properly.  